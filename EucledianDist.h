#pragma once
#include <iostream>

class Punct2D
{
private:
	double xcoord;
	double ycoord;

public:
	Punct2D(double xcoord, double ycoord);
	double euclidDist(Punct2D obj2);

};