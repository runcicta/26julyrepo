#pragma once
#include <iostream>

class ClockType
{
private:
	int hr;
	int min;
	int sec;
public:
	//ClockType();
	void setTime(int hr, int min, int sec);
	void getTime (int &hr, int &min, int &sec) const;
	void printTime() const;
	int incrementSeconds();
	int incrementMinutes();
	bool equalTime(const ClockType&) const;

};