#include "ClockType.h"

void ClockType::setTime(int hr, int min, int sec)
{
	if (hr > 24)
	{
		hr = 0;
	}
	if (min > 59)
	{
		min = 0;
	}
	if (sec > 59)
	{
		sec = 0;
	}

	this->hr = hr;//(*this).hr
	this->min = min;
	this->sec = sec;
	
}

void ClockType::getTime(int &hr, int &min, int &sec) const
{
	hr = this->hr;
	min = this->min;
	sec = this->sec;
}

void ClockType::printTime() const
{
	std::cout << "Hour:" << hr << std::endl;
	std::cout << "Minutes:" << min << std::endl;
	std::cout << "Seconds:" << sec << std::endl;
}

int ClockType::incrementSeconds()
{
	this->sec++;
	if (sec > 59)
	{
		this->min++;
	}
	return sec;
}

int ClockType::incrementMinutes()
{
	this->min++;
	if (min > 59)
	{
		this->hr++;
	}
	return min;
}

bool ClockType::equalTime(const ClockType& myclock) const
{
	if (myclock.hr == this->hr && myclock.min == this->min && myclock.sec == this->sec)
	{
		std::cout << "The clocks have the same time" << std::endl;
		return true;
	}
	else
	{
		std::cout << "The clocks have a different time" << std::endl;
		return false;
	}
}