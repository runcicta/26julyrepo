#include <math.h>
#include "EucledianDist.h"

double Punct2D::euclidDist(Punct2D obj2)
{
	double rezultat = 0.0;
	rezultat = sqrt(pow((obj2.ycoord - this->ycoord), 2) + pow((obj2.xcoord - this->xcoord), 2));
	return rezultat;
}

Punct2D::Punct2D(double xcoord, double ycoord)
{
	this->xcoord = xcoord;
	this->ycoord = ycoord;
}