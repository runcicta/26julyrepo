#include <iostream>
#include "ClockType.h"


int main()
{
	ClockType A;
	A.setTime(2, 3, 4);
	A.printTime();
	A.incrementMinutes();
	A.incrementSeconds();
	A.printTime();
	ClockType B;
	B.setTime(2, 3, 4);
	B.printTime();
	B.incrementMinutes();
	B.incrementSeconds();
	A.equalTime(B);
	return 0;
}